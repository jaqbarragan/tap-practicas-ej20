/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicas;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Jaqbarragan
 */
public class Practica1 extends JFrame implements ActionListener {

   JLabel etq1;
    JButton btn;
    JTextField nombre;
    
    Practica1(){
        this.setTitle("Practica 1");
        this.setSize(300, 150);
        this.setLayout(new FlowLayout());
        
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        etq1= new JLabel("Escriba un nombre para saludar");
        nombre = new JTextField(20);
        btn = new JButton("¡Saludar!");
        
        btn.addActionListener(this);
        
        this.add(etq1);
        this.add(nombre);
        this.add(btn);
    }
    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {                   
                new Practica1().setVisible(true);
            }
        });        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JOptionPane.showMessageDialog(this, "Hola! "+this.nombre.getText()); //To change body of generated methods, choose Tools | Templates.
    }
    
}
