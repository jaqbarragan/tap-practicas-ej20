
package practicas;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;

public class PracticadeModelado {
    public static void main(String[] args) {
        // TODO code application logic here
        JFrame frm = new JFrame("Ventana");
        frm.setSize(300,200);
        frm.setLayout(new FlowLayout());
        
        JButton btn = new JButton("Cerrar");
        
        btn.addActionListener((ActionEvent e) -> {
            System.exit(0);
        });
        
        frm.add(btn);
        
        frm.setVisible(true);
    }
    
}
