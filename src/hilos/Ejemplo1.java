
package hilos;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jaqbarragan
 */
public class Ejemplo1 implements Runnable 
{
    public static void main(String args[]){
       
      java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Ejemplo1().run();
            }
        });
    }
     Object semaforo = null;

    @Override
    public void run() {
        
        Hilo h1 = new Hilo("Hilo 1",15000);  //Hilo 1
        Hilo h2 = new Hilo("Hilo 2",2000);  //Hilo 2
        Hilo h3 = new Hilo("Hilo 3",2500);  //Hilo 3
        
        h1.start();
        h2.start();
        h3.start();
        
    for(int i=0;i<20;i++){
            System.out.printf("Ejecucion Principal, contador:%d \n",i);
            if(i==10){
                synchronized (this){
                  h1.interrupt();  
                }
                
            }
            try {
               Thread.sleep(3000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Ejemplo1.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
class Hilo extends Thread // implements Runnable     //Se ejecuta el programa con hilos
{
    String id = "";
    int miliseg = 1000;
    int x ;
    public Hilo(String id,int miliseg)
    {
        this.id = id;
        this.miliseg = miliseg;
    }
    
    @Override
    public void run() {
        for(int i=0;i<20;i++)
        {
            x = i;
            System.out.printf("Ejecucion de Tarea %s, contador:%d ,x=%d \n",this.id,i,x);
            try {
                Thread.sleep(this.miliseg);
            } catch (InterruptedException ex) {
                System.out.printf("La Tarea %s fue despertarda de su hilo",this.id);
//                Logger.getLogger(Ejemplo1.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    }    
}